# Кубок конфедерации

## Установка

```sh
$ git clone https://deeeeza@bitbucket.org/deeeeza/cubok.git #название папки
$ cd #название папки
$ npm install
$ gulp
PROFIT
```
### Разработка

```sh
$ gulp html:build - отдельно собирает html;
$ gulp js:build - отдельно собирает js;
$ gulp style:build - отдельно собирает css;
$ gulp fonts:build - отдельно собирает fonts;
$ gulp image:build - отдельно собирает image;
$ gulp build - собирает проект-продакшн в папку build;
$ gulp clean - удаляет папку build;
$ gulp - собирает проект, запускает сервер;
```
