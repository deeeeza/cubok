$(".btn__buy").click(function(t) {
	t.preventDefault(); 
	$(".overlay").fadeIn(400, function() {
		console.log('click');
		$(".popup__confirmation").css("display", "block").animate({
			opacity: 1,
			top: "45%"
		}, 200), $(".top__line").css("z-index", "100000")
	})
}), $(".popup_close, #overlay").click(function() {
	$(".popup__confirmation").animate({
		opacity: 0,
		top: "45%"
	}, 200, function() {
		$(this).css("display", "none"), $(".overlay").fadeOut(400), $(".top__line").css("z-index", "1000000")
	})
});



$(".slide__btn").click(function(t) {
	t.preventDefault(); 
	$(".overlay").fadeIn(400, function() {
		console.log('click');
		$(".slide__wrapper").css("display", "none");
		$(".slide .popup_close").css("display", "none");
		$(".slide__popup__confirmation").css("display", "block").animate({
			opacity: 1,
			top: "45.8%",
			right: "10%"
		}, 200), $(".top__line").css("z-index", "100000")
	})
}), $(".popup_close, #overlay").click(function() {
	$(".slide__wrapper").css("display", "block");
	$(".slide .popup_close").css("display", "block");
	$(".slide__popup__confirmation").animate({
		opacity: 0,
		top: "45.8%",
		right: "0"
	}, 200, function() {
		$(this).css("display", "none"), $(".overlay").fadeOut(400), $(".top__line").css("z-index", "1000000")
	})
});